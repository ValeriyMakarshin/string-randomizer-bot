import logging
import os
import time
from concurrent.futures.thread import ThreadPoolExecutor

from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext

from formatter import process_message

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

message_buffer = None
executor = ThreadPoolExecutor(1)


def thread_function(update: Update):
    global message_buffer

    time.sleep(1)
    current_message = message_buffer
    message_buffer = None
    if current_message is not None:
        formatted_text = process_message(current_message)
        update.message.reply_text(formatted_text if len(formatted_text) > 0 else "*Empty*")


def start(update: Update, context: CallbackContext) -> None:
    update.message.reply_text('Hi!')


def echo(update: Update, context: CallbackContext) -> None:
    message = update.message.text

    global message_buffer
    if message_buffer is None:
        message_buffer = message
    else:
        message_buffer = "{}\n{}".format(message_buffer, message)

    executor.submit(thread_function, update)


def main():
    updater = Updater(os.getenv("STRING_RANDOMIZER_TOKEN"))
    dispatcher = updater.dispatcher
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, echo))
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
