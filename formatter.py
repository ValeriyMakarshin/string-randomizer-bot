import random
import re


def process_message(message: str) -> str:
    items = message.splitlines()
    sorted_list = list()
    for item in items:
        tripped_string = item.strip()
        find_list = re.findall("^[А-Яа-я0-9,. \t-]+$", tripped_string)
        if len(find_list) > 0:
            sorted_list.append(find_list[0])
    random.shuffle(sorted_list)
    return "\n".join(sorted_list)


if __name__ == '__main__':
    r = process_message("""
        suddenly
        seizure
        distinguish
        conurbation
        aversion
        
        miserable
        severe
        ward
        tightly
        gently
        
        desirable
        versatile
        brings
        continuously
        unveils
        =============
        вдруг, внезапно
        Приступ
        различать
        пригород
        отвращение
        
        несчастный
        суровый
        подопечный
        плотно
        нежно
        
        желательно
        разносторонний
        приносит
        непрерывно
        анонсирует
        """)
    print(r)
